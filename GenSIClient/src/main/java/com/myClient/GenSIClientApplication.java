package com.myClient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author ：qin
 * @date ：Created in 2021/5/6
 * @description:
 **/
@SpringBootApplication
public class GenSIClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(GenSIClientApplication.class,args);
    }
}
